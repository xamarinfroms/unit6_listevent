﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit5.Core;

namespace Unit5_DataBinding
{
    public class ProductViewModel: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ProductViewModel(Product data)
        {
            this._data = data;
        }

        public string Name
        {
            get { return this._data.Name; }
            set { this._data.Name = value; this.FireUpdate(nameof(this.Name)); }
        }


        public override string ToString()
        {
            return this.Name;
        }

        public void FireUpdate(string name)
        {
            this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(name));
        }

        private Product _data;
    }
}
